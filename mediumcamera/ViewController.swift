//
//  ViewController.swift
//  mediumcamera
//
//  Created by Vlad on 08/07/2019.
//  Copyright © 2019 Vlad. All rights reserved.
//

import UIKit
import AVFoundation
class ViewController: UIViewController {
    
    @IBOutlet weak var previewView: UIImageView!

    var session: AVCaptureSession?
    var stillImageOutput: AVCapturePhotoOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        
        let camera = AVCaptureDevice.default(.builtInWideAngleCamera,  for: AVMediaType.video, position: .back)
        var input: AVCaptureDeviceInput!
        var error: NSError?
        do {
            input = try AVCaptureDeviceInput(device: camera!)
        } catch let error1 as NSError
        {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session?.addInput(input)
            stillImageOutput = AVCapturePhotoOutput()
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey:"AVVideoCodecJPEG.jpg"])
            stillImageOutput?.setPreparedPhotoSettingsArray([settings], completionHandler: nil)
            //stillImageOutput?.outputSettings =
            
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                //videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayer!.frame = previewView.bounds
    }


}

